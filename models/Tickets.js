var mongoose = require('mongoose');
var TicketScheme = new mongoose.Schema({
    id: Number,
    subject: String,
    status: String,
    comment: String,
    updated_date: { type: Date, default: Date.now },
  });

  module.exports = mongoose.model('Ticket', TicketScheme);
