var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Ticket = require('../models/Tickets.js');

/* GET ALL TicketS */
router.get('/', function(req, res, next) {
  Ticket.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* GET SINGLE Ticket BY ID */
router.get('/:id', function(req, res, next) {
  Ticket.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE Ticket */
router.post('/', function(req, res, next) {
  Ticket.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* UPDATE Ticket */
router.put('/:id', function(req, res, next) {
  Ticket.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE Ticket */
router.delete('/:id', function(req, res, next) {
  Ticket.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;